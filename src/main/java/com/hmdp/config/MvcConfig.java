package com.hmdp.config;

import com.hmdp.utils.LoginInterceptor;
import com.hmdp.utils.RefreshInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor())
				.excludePathPatterns(
						"/shop-type/**",
						"/shop/**",
						"/user/code",
						"/user/login",
						"/blog/hot",
						"/upload/**",
						"/voucher/**",
						"/voucher-order/**"
				)
				.order(1);
		registry.addInterceptor(new RefreshInterceptor(stringRedisTemplate))
				.addPathPatterns("/**")
				.order(0);
		//值越小，优先级越高，越早执行
	}
}
