package com.hmdp.utils;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.BooleanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class SimpleRedisLock implements ILock {

	private StringRedisTemplate stringRedisTemplate;
	private String name;
	private static final String KEY_PREFIX = "lock:";
	private static final String ID_PREFIX = UUID.randomUUID().toString(true) + "-";


	public SimpleRedisLock(String name, StringRedisTemplate stringRedisTemplate) {
		this.name = name;
		this.stringRedisTemplate = stringRedisTemplate;
	}

	@Override
	public boolean tryLock(long timestamp) {
		//获取线程标识
		String threadId = ID_PREFIX + Thread.currentThread().getId();
		String key = KEY_PREFIX + name;
		//获取锁
		Boolean success = stringRedisTemplate.opsForValue().setIfAbsent(key, threadId, timestamp, TimeUnit.SECONDS);
		//拆箱 有可能为null  使用工具判断
		return BooleanUtil.isTrue(success);
	}

	@Override
	public void unLock() {
		String threadId = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
		if (Objects.equals(threadId,ID_PREFIX + Thread.currentThread().getId())){
			stringRedisTemplate.delete(KEY_PREFIX + name);
		}
	}
}
