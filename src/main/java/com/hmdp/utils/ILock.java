package com.hmdp.utils;

public interface ILock {

	 boolean tryLock(long timestamp);
	 void unLock();
}
