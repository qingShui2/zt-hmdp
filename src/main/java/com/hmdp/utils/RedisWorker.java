package com.hmdp.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Component
public class RedisWorker {

	//开始时间戳
	private static final long BEGIN_TIMESTAMP = 1640995200L;
	//序列号位数
	private static final int COUNT_BITS = 32;

	private final StringRedisTemplate stringRedisTemplate;

	public RedisWorker(StringRedisTemplate stringRedisTemplate) {
		this.stringRedisTemplate = stringRedisTemplate;
	}

	//生成唯一ID
	public long nextId(String keyPrefix) {
		//1生成时间戳
		LocalDateTime now = LocalDateTime.now();
		long l = now.toEpochSecond(ZoneOffset.UTC);
		long timestamp = l - BEGIN_TIMESTAMP;
		//2生成序列号,业务前缀加上当天日期
		String date = now.format(DateTimeFormatter.ofPattern("yyyy:MM:dd"));
		long count = stringRedisTemplate.opsForValue().increment("irc:" + keyPrefix + ":" + date);

		//3拼成一个ID
		return timestamp << COUNT_BITS | count;
	}

	public static void main(String[] args) {
		LocalDateTime time = LocalDateTime.of(2022, 1, 1, 0, 0, 0);
		long l = time.toEpochSecond(ZoneOffset.UTC);
		System.out.println(l);
	}


}
