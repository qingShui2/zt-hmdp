package com.hmdp.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.hmdp.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class RefreshInterceptor implements HandlerInterceptor {

	private StringRedisTemplate stringRedisTemplate;

	public RefreshInterceptor(StringRedisTemplate stringRedisTemplate) {
		this.stringRedisTemplate = stringRedisTemplate;
	}


	//登录拦截，排除登录接口
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		log.info("登录拦截");
		//获取token
		String token = request.getHeader("authorization");
		if (StrUtil.isEmpty(token)) {
			return true;
		}
		//拿到redis中的用户
//		String key = RedisConstants.LOGIN_CODE_KEY + token;
		Map<Object, Object> userMap = stringRedisTemplate.opsForHash().entries(token);
		//不存在就拦截
		if (userMap.isEmpty()) {
			return true;
		}
		//存在保存在本线程
		UserDTO userDTO = BeanUtil.fillBeanWithMap(userMap, new UserDTO(), false);
		UserHolder.saveUser(userDTO);
		//刷新存储时长
		stringRedisTemplate.expire(token, RedisConstants.LOGIN_USER_TTL, TimeUnit.SECONDS);
		log.info("登录成功!已刷新保存时间");
		//放行
		return true;
	}


	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		//移除用户
		UserHolder.removeUser();
	}
}
