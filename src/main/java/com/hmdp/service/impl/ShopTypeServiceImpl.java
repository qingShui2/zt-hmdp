package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public Result queryList() {
		// 1. 从redis中查询商铺类型列表
//		String jsonArray = stringRedisTemplate.opsForValue().get(RedisConstants.CACHE_TYPE_KEY);
		// 1list取出
		List<String> jsonArray = stringRedisTemplate.opsForList().range(RedisConstants.CACHE_TYPE_KEY, 0, -1);
		// json转list
		List<ShopType> jsonList = new ArrayList<>();
		for (String s : jsonArray) {
			ShopType shopType = JSONUtil.toBean(s, ShopType.class);
			jsonList.add(shopType);
		}
		// 2. 命中，返回redis中商铺类型信息
		if (!CollectionUtils.isEmpty(jsonList)) {
			return Result.ok(jsonList);
		}
		// 3. 未命中，从数据库中查询商铺类型,并根据sort排序
		List<ShopType> shopTypesByMysql = query().orderByAsc("sort").list();
		System.out.println("mysql" + shopTypesByMysql);
		// 4. 将商铺类型存入到redis中
//		stringRedisTemplate.opsForValue().set(RedisConstants.CACHE_TYPE_KEY, JSONUtil.toJsonStr(shopTypesByMysql));
		// 4 使用List类型存储
		for (ShopType shopType : shopTypesByMysql) {
			String s = JSONUtil.toJsonStr(shopType);
			stringRedisTemplate.opsForList().rightPush(RedisConstants.CACHE_TYPE_KEY, s);
		}
		stringRedisTemplate.expire(RedisConstants.CACHE_TYPE_KEY, RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);
		// 5. 返回数据库中商铺类型信息
		return Result.ok(shopTypesByMysql);
	}
}
