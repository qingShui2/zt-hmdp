package com.hmdp.service.impl;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisConstants;
import com.hmdp.utils.RedisData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.CACHE_SHOP_KEY;
import static com.hmdp.utils.RedisConstants.LOCK_SHOP_KEY;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {

	private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Override
	public Result queryById(Long id) {
		//缓存穿透
		//Shop shop = queryByIdPassThrough(id);

		//缓存击穿，互斥锁保护数据库。
//		Shop shop = queryByIdWithMutex(id);

		//逻辑删除
		Shop shop = queryByIdLogicalExpire(id);
		if (shop == null) {
			return Result.fail("店铺不存在");
		}
		return Result.ok(shop);
	}

	public Shop queryByIdLogicalExpire(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//先查redis 没有则返回null
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		if (StrUtil.isBlank(shopJson)) {
			return null;
		}
		//逻辑过期时间，新线程同步数据库
		RedisData redisData = JSONUtil.toBean(shopJson, RedisData.class);
		JSONObject jsonObject = (JSONObject) redisData.getData();
		Shop shop = JSONUtil.toBean(jsonObject, Shop.class);
		//3 如果有数据，时间没有过期，返回
		LocalDateTime expireTime = redisData.getExpireTime();
		if (expireTime.isAfter(LocalDateTime.now())) {
			return shop;
		}

		//4 时间过期，抢锁，开启新的线程执行同步 释放锁，返回数据
		String lockKey = RedisConstants.LOCK_SHOP_KEY + id;
		boolean b = tryLock(lockKey);
		if (b) {
			CACHE_REBUILD_EXECUTOR.submit(() -> {
				try {
					this.saveData2Redis(id, 20L);
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					unLock(lockKey);
				}
			});
		}

		return shop;
	}


	//缓存穿透
	public Shop queryByIdPassThrough(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//先查redis 有则返回
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		if (StrUtil.isNotBlank(shopJson)) {
			Shop shop = JSONUtil.toBean(shopJson, Shop.class);
			return shop;
		}
		//如果返回的是空白，空白不等于null,不允许查数据库。
		if (shopJson != null) {
			return null;
		}

		//没有再查数据库
		Shop shop = baseMapper.selectById(id);
		if (shop == null) {
			//数据库没有，添加空白缓存，返回fail
			stringRedisTemplate.opsForValue().set(key, "", RedisConstants.CACHE_NULL_TTL, TimeUnit.MINUTES);
			return null;
		}
		//数据库有，返回并且更新到redis中
		log.info("商品:" + id + ",已更新至redis缓存");
		stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);

		return shop;
	}


	//缓存击穿,互斥锁查询
	public Shop queryByIdWithMutex(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//先查redis 有则返回
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		if (StrUtil.isNotBlank(shopJson)) {
			Shop shop = JSONUtil.toBean(shopJson, Shop.class);
			return shop;
		}
		//如果返回的是空白，空白不等于null,也不查数据库。
		if (shopJson != null) {
			return null;
		}
		//没有再查数据库,开始缓存重建
		//尝试索取锁
		String lockKey = LOCK_SHOP_KEY + id;
		Shop shop = null;
		try {
			boolean b = tryLock(lockKey);
			if (!b) {
				//拿锁失败
				Thread.sleep(50);
				return queryByIdWithMutex(id);
			}
			shop = baseMapper.selectById(id);
			Thread.sleep(200);
			if (shop == null) {
				//数据库没有，添加空白缓存，返回fail
				stringRedisTemplate.opsForValue().set(key, "", RedisConstants.CACHE_NULL_TTL, TimeUnit.MINUTES);
				return null;
			}
			//数据库有，返回并且更新到redis中
			log.info("商品:" + id + ",已更新至redis缓存");
			stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			unLock(lockKey);
		}
		return shop;
	}

	//添加互斥锁
	public boolean tryLock(String key) {
		Boolean aBoolean = stringRedisTemplate.opsForValue().setIfAbsent(key, "", 10, TimeUnit.SECONDS);
		return BooleanUtil.isTrue(aBoolean);
	}

	//释放互斥锁
	public void unLock(String key) {
		stringRedisTemplate.delete(key);
	}

	//逻辑过期，同步
	public void saveData2Redis(Long id, Long expireSeconds) throws InterruptedException {
		Shop shop = baseMapper.selectById(id);
		RedisData<Shop> redisData = new RedisData<>();
		redisData.setData(shop);
		redisData.setExpireTime(LocalDateTime.now().plusSeconds(expireSeconds));
		String s = JSONUtil.toJsonStr(redisData);
		stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(redisData));
	}

	@Override
	@Transactional
	public Result update(Shop shop) {
		//更新数据库
		baseMapper.updateById(shop);
		Long id = shop.getId();
		if (id == null) {
			return Result.fail("店铺ID不能为空");
		}
		//删除redis
		stringRedisTemplate.delete(CACHE_SHOP_KEY + shop.getId());
		//返回数据
		return Result.ok();
	}
}
