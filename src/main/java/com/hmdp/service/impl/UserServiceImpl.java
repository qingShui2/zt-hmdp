package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.LoginFormDTO;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.User;
import com.hmdp.mapper.UserMapper;
import com.hmdp.service.IUserService;
import com.hmdp.utils.RedisConstants;
import com.hmdp.utils.RegexUtils;
import com.hmdp.utils.SystemConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.hmdp.utils.RedisConstants.LOGIN_CODE_TTL;
import static com.hmdp.utils.RedisConstants.LOGIN_USER_TTL;
import static com.hmdp.utils.RegexPatterns.VERIFY_CODE_REGEX;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

	@Autowired
	private StringRedisTemplate stringRedisTemplate;


	@Override
	public Result sendCode(String phone, HttpSession session) {
		//验证手机号
		boolean phoneInvalid = RegexUtils.isPhoneInvalid(phone);
		if (!phoneInvalid) {
			//生成验证码并发送
			String code = RandomUtil.randomNumbers(6);
			//验证码保存在redis中
			stringRedisTemplate.opsForValue().set(RedisConstants.LOGIN_CODE_KEY + phone, code, RedisConstants.LOGIN_CODE_TTL, TimeUnit.MINUTES);
			log.info("发送成功");
			return Result.ok(code);
		} else {
			return Result.fail("请输入正确的手机号");
		}
	}

	@Override
	public Result login(LoginFormDTO loginForm, HttpSession session) {
		//校验手机号
		String phone = loginForm.getPhone();
		boolean phoneInvalid = RegexUtils.isPhoneInvalid(phone);
		//手机号正确
		if (phoneInvalid) {
			return Result.fail("请输入有效手机号码");
		}
		//通过phone查找用户
		String cacheCode = stringRedisTemplate.opsForValue().get(RedisConstants.LOGIN_CODE_KEY + phone);
		if (cacheCode == null || !cacheCode.equals(loginForm.getCode())) {
			//验证码不一致
			return Result.fail("验证码错误");
		}
		//验证通过,查找用户
		LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(User::getPhone, phone);
		User user = baseMapper.selectOne(queryWrapper);
		//不存在就创建
		if (user == null) {
			user = createUserWithPhone(phone);
		}
		//随机生成一个token，使用hutool工具
		String token = UUID.randomUUID().toString(true);
		//user对象存为hash
		UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
		//存到redis中
		HashMap<String, String> stringObjectMap = new HashMap<>();
		String userId = Convert.toStr(userDTO.getId());
		stringObjectMap.put("id", userId);
		stringObjectMap.put("nickName", userDTO.getNickName());
		stringObjectMap.put("icon", userDTO.getIcon());
		token = RedisConstants.LOGIN_CODE_KEY + token;
		stringRedisTemplate.opsForHash().putAll(token, stringObjectMap);
		//设置有效期
		stringRedisTemplate.expire(token, LOGIN_USER_TTL, TimeUnit.SECONDS);
		log.info(user.toString());
		return Result.ok(token);
	}

	private User createUserWithPhone(String phone) {
		User user = new User();
		user.setPhone(phone);
		user.setNickName(SystemConstants.USER_NICK_NAME_PREFIX + RandomUtil.randomString(10));
		save(user);
		return user;
	}


}
