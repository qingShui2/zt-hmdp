package com.hmdp.service.impl;

import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.SeckillVoucher;
import com.hmdp.entity.VoucherOrder;
import com.hmdp.mapper.VoucherOrderMapper;
import com.hmdp.service.ISeckillVoucherService;
import com.hmdp.service.IVoucherOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisWorker;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.concurrent.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
@Slf4j
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {

	@Autowired
	private ISeckillVoucherService seckillVoucherService;

	@Autowired
	private RedisWorker redisWorker;
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	@Autowired
	private RedissonClient redissonClient;

	private static final DefaultRedisScript<Long> SECKILL_SCRIPT;

	static {
		SECKILL_SCRIPT = new DefaultRedisScript<>();
		SECKILL_SCRIPT.setLocation(new ClassPathResource("seckill.lua"));
		SECKILL_SCRIPT.setResultType(Long.class);
	}

	//创建一个阻塞队列
	private BlockingQueue<VoucherOrder> orderTasks = new ArrayBlockingQueue<>(1024 * 102);
	//创建一个线程池处理阻塞队列的任务
	private static final ExecutorService SECKILL_ORDER_EXECUTOR = Executors.newSingleThreadExecutor();


	//初始化完成之后立即执行
	@PostConstruct  //后置处理器
	private void init() {
		SECKILL_ORDER_EXECUTOR.submit(new VoucherOrderHandler());
	}

	private class VoucherOrderHandler implements Runnable {
		@Override
		public void run() {
			while (true) {
				try {
					log.info("0000");
					VoucherOrder voucherOrder = orderTasks.take();
					log.info("1231231213314133" + voucherOrder);
					handlerVoucherOrder(voucherOrder);

				} catch (Exception e) {
					log.info("订单处理异常:" + e);
				}

			}
		}
	}

	private void handlerVoucherOrder(VoucherOrder voucherOrder) {
		log.info("111111");
		Long userId = voucherOrder.getUserId();
		log.info("222222");

		RLock lock = redissonClient.getLock("lock:order:" + userId);
		log.info("33333");
		//尝试获取锁
		boolean isLock = lock.tryLock();
		if (!isLock) {
			//获取锁失败 既然失败，说明一个用户多次尝试，不符合实际情况
			log.info("不允许重复下单");
			return;
		}
		log.info("4444");
		try {
			log.info("5555");
			//获取代理对象 创建订单
//			IVoucherOrderService voucherOrderService = (IVoucherOrderService) AopContext.currentProxy();//子线程拿不到主线程内容
			proxy.createVoucherOrder(voucherOrder);
		} finally {
			lock.unlock();
		}
	}

	private IVoucherOrderService proxy;

	@Override
	public Result seckillVoucher(Long voucherId) {
		String voucherIdStr = voucherId.toString();
		UserDTO user = UserHolder.getUser();
		//执行lua脚本
		Long result = stringRedisTemplate.execute(SECKILL_SCRIPT, Collections.emptyList(), voucherIdStr, user.toString());
		//判断结果1 2 0
		assert result != null;
		int r = result.intValue();
		switch (r) {
			case 1:
				return Result.fail("活动结束");
			case 2:
				return Result.fail("重复下单");
			default:
				//创建优惠卷订单，放到阻塞队列 写入数据库
				VoucherOrder voucherOrder = new VoucherOrder();
				long orderId = redisWorker.nextId("order");
				log.info(String.valueOf(orderId));
				voucherOrder.setVoucherId(voucherId);
				voucherOrder.setUserId(user.getId());
				voucherOrder.setId(orderId);
				orderTasks.add(voucherOrder);
				proxy = (IVoucherOrderService) AopContext.currentProxy();
				return Result.ok(orderId);
		}
	}


	/*
	@Override
	public Result seckillVoucher(Long voucherId) {
		//查询优惠券
		SeckillVoucher voucher = seckillVoucherService.getById(voucherId);
		Integer stocked = voucher.getStock();
		//是否开始
		LocalDateTime beginTime = voucher.getBeginTime();
		//true未开始
		boolean before = LocalDateTime.now().isBefore(beginTime);
		if (before) {
			return Result.fail("未开始");
		}
		//是否结束
		LocalDateTime endTime = voucher.getEndTime();
		boolean after = LocalDateTime.now().isAfter(endTime);
		if (after) {
			return Result.fail("已结束");
		}
		//返回订单ID
//		Long id = UserHolder.getUser().getId();
//		synchronized (id.toString().intern()) {
//			IVoucherOrderService voucherOrderService = (IVoucherOrderService) AopContext.currentProxy();
//			return voucherOrderService.createVoucherOrder(voucherId);
//		}
		Long id = UserHolder.getUser().getId();
//		SimpleRedisLock simpleRedisLock = new SimpleRedisLock("order:" + id, stringRedisTemplate);

		RLock lock = redissonClient.getLock("lock:order:" + id);
		//尝试获取锁
		boolean isLock = lock.tryLock();
		if (!isLock) {
			//获取锁失败 既然失败，说明一个用户多次尝试，不符合实际情况
			return Result.fail("不允许重复下单");
		}
		try {
			//获取代理对象 创建订单
			IVoucherOrderService voucherOrderService = (IVoucherOrderService) AopContext.currentProxy();
			return voucherOrderService.createVoucherOrder(voucherId);
		} catch (IllegalStateException e) {
			throw new RuntimeException("抢卷失败");
		} finally {
			lock.unlock();
		}

	}*/

	@Transactional
	public void createVoucherOrder(VoucherOrder voucherOrder) {
		Long userId = voucherOrder.getUserId();
		Long voucherId = voucherOrder.getVoucherId();

		Integer count = query().eq("user_id", userId).eq("voucher_id", voucherId).count();
		if (count > 0) {
			log.info("您已成功抢到");
			return;
		}

		//扣除库存
		boolean success = seckillVoucherService.update()
				.setSql("stock = stock -1")
				.eq("voucher_id", voucherId)
				.gt("stock", 0).update();
		if (!success) {
			log.info("库存不足！");
			return;
		}
		//创建订单
		save(voucherOrder);
	}


}
