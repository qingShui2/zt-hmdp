package com.hmdp;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.hmdp.entity.ShopType;
import com.hmdp.service.impl.ShopServiceImpl;
import com.hmdp.utils.RedisWorker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@SpringBootTest
class HmDianPingApplicationTests {

	@Autowired
	private ShopServiceImpl shopService;

	@Autowired
	private RedisWorker redisWorker;

	private ExecutorService es = Executors.newFixedThreadPool(500);

	@Test
	void test() {
		ShopType shopType = new ShopType();
		shopType.setId(23L);
		shopType.setName("名字");
		shopType.setSort(23);
		//对象打印tostring
		String s1 = shopType.toString();
		System.out.println("直接打印对象：" + s1);
		ArrayList<ShopType> objects = new ArrayList<>();
		ShopType shopType1 = new ShopType();
		shopType1 = shopType;
		objects.add(shopType);
		objects.add(shopType1);
		System.out.println("直接打印一个对象的集合：" + objects);
		//对象转json
		String s = JSONUtil.toJsonStr(shopType);
		System.out.println("打印对象的json：" + s);
		String s2 = JSONUtil.toJsonStr(objects);
		System.out.println("打印一个对象的集合json：" + s2);
		//普通对象字符串转object

		List<ShopType> shopTypeList = JSONUtil.toList(s2, ShopType.class);
		System.out.println("JSON转换成集合:" + shopTypeList);
	}

	@Test
	void kkk() throws InterruptedException {
		shopService.saveData2Redis(1L, 10L);
	}

	@Test
	void KKK() throws InterruptedException {
		CountDownLatch countDownLatch = new CountDownLatch(300);
		Runnable task = () -> {
			for (int i = 0; i < 100; i++) {
				long id = redisWorker.nextId("order");
				System.out.println(id);
			}
			countDownLatch.countDown();
		};
		long l = System.currentTimeMillis();
		for (int i = 0; i < 300; i++) {
			es.submit(task);
		}
		countDownLatch.await();
		long r = System.currentTimeMillis();
		System.out.println(r-l);

	}

}
